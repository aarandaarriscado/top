#!/usr/bin/env ruby
# Encoding: utf-8
require 'rubygems'
require 'open4'
require 'fileutils'
require_relative 'md5.rb'
class Metadatavideo   
    attr_accessor :duration
    attr_accessor :format
    attr_accessor :width
    attr_accessor :height
    attr_accessor :size
    attr_accessor :vbitrate
    attr_accessor :vcodec
    attr_accessor :vframerate
    attr_accessor :abitrate
    attr_accessor :acodec
    attr_accessor :asamplerate 
    attr_accessor :achannels
    attr_accessor :size
    attr_accessor :rotation
    attr_accessor :md5


    def initialize(path,template,templaterotation,size,pathxml,fileconfig,log)  
     # Instance variables  
	 cmd="mediainfo --Inform=file:" +  template + " " + "'" + path + "'"
 	 log.info ("Obteniendo la informacion de origen del archivo, ejecutando comando " + cmd)
         pid, stdin, stdout, stderr = Open4::popen4 cmd
         ignored, status = Process::waitpid2 pid
         mediainfo = stdout.read.strip
         medianfolength = mediainfo.length
	 value = mediainfo.split(",")
	 puts value.length
	 puts value[1]

	 if value.length > 6 and ( size == value[1] or size == "0" ) then
		@duration = value[0]
                @size = value[1]
		@format = value[2]
		@width = value[5]
		@height = value[6]
		@vbitrate = value[4]
		@vcodec = value[3]
		@vframerate = value[7]
		@abitrate = value[9]
		@acodec = value[8]
		@asamplerate = value[11]
		@achannels = value[10]
		
	 else
		if size == value[0] then
			log.error  ("formato del archivo  de video incorrecto para el xml " + File.basename(pathxml))
			puts "mv "  +  pathxml + "  , " +  fileconfig["direrror"] + File.basename(pathxml)
			FileUtils.mv    pathxml  , fileconfig["direrror"] + File.basename(pathxml)
			Thread.exit
		else 
			log.error ("el tamano del fichero de entrada no coincide con el especificado para el xml " + File.basename(pathxml))
			puts "mv "  +  pathxml + "  , " +  fileconfig["direrror"] + File.basename(pathxml)
			FileUtils.mv    pathxml  , fileconfig["direrror"] + File.basename(pathxml)
			Thread.exit
		end
	 end 
	 log.info ("valor del md5")
	 p "valor del md5"
	 a = MD5.new(path)
	 @md5 = a.md5
	 log.info (@md5)
	 p a.md5	
	
    end #### Finalizacion Initialize 
  
end  
