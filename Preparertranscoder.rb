#!/usr/bin/env ruby
# Encoding: utf-8
require 'rubygems'
require 'open4'
require 'fileutils'
require 'open3'
require 'date'
require_relative 'GenerateCommand'
class Preparertranscoder   
    attr_accessor :renditions
    attr_accessor :paths
    attr_accessor :monthpath
    attr_accessor :daypath
    attr_accessor :count
    
    def initialize(video,fileconfig,input,sourcedata,log)  
     # Instance variables 
	    #
	puts "buscando valor de la variable assetvideoencodemultiple"
	puts  video.assetvideoencodemultiple
	if input == nil then

                numassets = []
                urlsconf = []
        else

		 if video.logo == "TRUE" then

                        if video.assetvideoencodemultiple == "TRUE" then

                                numassets = fileconfig["MULTIPLELOGO"]
                        else
                                if video.assetvideoencodeto == "none" and video.accountname == "elpais" then
                                        log.info "Singlelogocopy"
                                        numassets = fileconfig["SINGLELOGOCOPY"]
                                else
                                        numassets = fileconfig["SINGLELOGO"]
                                end
                        end

                 else

                        if video.assetvideoencodemultiple == "TRUE" then

                                numassets = fileconfig["MULTIPLE"]
                        else
                                numassets = fileconfig["SINGLE"]
                        end
                 end
	 end	

	 i=0
	 c=0
         @renditions = []
	 fecha = Date.today
	 urlmonthpath = fecha.year.to_s +  fecha.month.to_s
	 urldaypath = fecha.day.to_s
	 #uploadcompletepath = fileconfig["uploadpath"] +  urlmonthpath  + "/" + fecha.day.to_s 
	 uploadcompletepath = fileconfig["uploadpath"] +   urlmonthpath  + "/" + fecha.day.to_s 
	 while i < numassets.count 
	 	data = fileconfig[numassets[i]]

		#if data[11].start_with?('video') then			
		#	log.info "El video tiene un width de " + sourcedata.width + " y vamos a generar una rendition de " +  data[5].to_i.to_s 
		size = " -s " + data[1].to_i.to_s + "x" + data[2].to_i.to_s + " " 
		#else
		#	size = ""
		#end
	        	 
		puts data
		command = GenerateCommand.new(data,fileconfig,input,sourcedata,size) 

		#log.info "El video tiene un width de " + sourcedata.vbitrate.to_i.to_s + " y vamos a generar una rendition de " +  (data[3].to_i/1000).to_s 
		#if c > 0 and data[3].to_i/1000 > sourcedata.vbitrate.to_i then
		#	i = i + 1 
		#	next
		#end
		#puts command
			
		###Generamos las urls
		urlsconf = fileconfig["url"]
		u = 0
		url=[]
		type = []
		while u < urlsconf.count

			url[u] = {"url" => fileconfig[urlsconf[u]][0] + urlmonthpath + "/" + urldaypath + "/" + File.basename(command.output) + fileconfig[urlsconf[u]][1] , "typeUrl" => fileconfig[urlsconf[u]][2]}
			#puts fileconfig[urlsconf[u]][1] 
			u = u +1
		end
		### Terminamos de generar las urls
		@renditions[c] = {"idref" =>video.refidvideo , "account" => video.publiserid, "accountname" => video.accountname, "command" => command.command, "src" => File.basename(input) , "dst" => command.output , "vbitrate" => data[3] ,"vcodec" => data[4] , "mimetype" => data[11] , "container" => data[2] ,"width" => data[5], "height" => data[6] , "size" => "" ,  "acodec" => data[7], "abitrate" => data[8], "achannels" => data[9], "asamplerate" => data[10],  "uploadcompletepath" => uploadcompletepath , "numassets" => numassets.count, "urlmonthpath" => urlmonthpath , "urldaypath" => urldaypath}
		i = i + 1
	        c = c + 1	 
	 end ###Fin del while


		

	 	u = 0 
		while u < urlsconf.count
			urlbase = fileconfig[urlsconf[u]][0] + urlmonthpath + "/" + urldaypath + "/" + File.basename(input,File.extname(input)) + "_"
                        urlext = "." + data[4] + fileconfig[urlsconf[u]][1]
                        type[u] = {"urlbase" => urlbase , "urlext" => urlext }
			u = u + 1

		end
		@paths = {"type" => type}
	        @monthpath = urlmonthpath
		@daypath = urldaypath	
		@count = c
    end #### Finalizacion Initialize 
  
end

