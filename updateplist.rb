#!/usr/bin/env ruby
require 'sinatra/base'
require 'resque'
require 'open3'
require 'resque/server'
require 'resque/status_server'
require 'resque/job_with_status'
require 'xmlsimple'
require 'fileutils'
require 'net/ftp'
require 'date'
require 'json'
require_relative 'hsconfig.rb'
require_relative 'Calculeplaylist.rb'
require_relative 'Enviamail'
class Updateplist 
  include Resque::Plugins::Status

   def update_plist(options,fileconfig,log)
                                ##Actualizamos la rendition

                                urlsconf = fileconfig["url"]
                                url = []
                                type = []
                                u = 0
				
				if fileconfig["provider"] == '1'
				   if options["data"] != nil then ####Venimos por la rama del geobloqueo
                                	while u < urlsconf.count

                          	              urlbase = fileconfig[urlsconf[u]][0] +  options["data"]["urlmonthpath"] + "/" + options["data"]["urldaypath"] + "/" + File.basename(options["data"]["src"],File.extname(options["data"]["src"])) + "_"
                                	      urlext =  File.extname(options["data"]["src"]) + fileconfig[urlsconf[u]][1]
                                       	      type[u] = {"urlbase" => urlbase , "urlext" => urlext }
                                       	      u = u + 1

                	                end
				   else
					 while u < urlsconf.count

                                              urlbase = fileconfig[urlsconf[u]][0] + options["month"] + "/" + options["day"] + "/" + File.basename(options["filename"],File.extname(options["filename"])) + "_"
                                              urlext =  File.extname(options["filename"]) + fileconfig[urlsconf[u]][1]
                                              type[u] = {"urlbase" => urlbase , "urlext" => urlext }
                                              u = u + 1

                                        end
				   end
				end	
								metadatafile = fileconfig["uploadjson"] + options["idref"] + ".json"
                                playlist = Calculeplaylist.new(type,metadatafile,options["assetscount"])
								hash = JSON.parse(File.read(metadatafile))
				if fileconfig["provider"] == '1' then ### Unicamente calculamos playlist con el provider de prisadigital
					plist = [ {"url" => playlist.playlistHDS , "typeUrl" => "1" } , { "url" => playlist.playlistIOS ,  "typeUrl" => "2" } ]
				else
					plist = []
				end
                if  playlist.complete  == 1 
					if playlist.first == 1 then
						hash["endOfProcess"] = Time.now.to_i
																					
					end
					hash["status"] = "4"
					
				else
					
					if playlist.first == 1 then
						hash["status"] = "6"	
						hash["endOfProcess"] = Time.now.to_i
					else
						hash["status"] = "6"
										
					end
					
				end
				hash["idref"] =  options["idref"]
				hash["playlist"] = plist
                log.info ("Realizamos el put para cambiar el status en el updateplaylist")
				
				
				
				
				                      
                                
				
				
				File.open(metadatafile,"w") do |f|
					f.puts hash.to_json
				end	
				



   end

   

   def perform
        encoding_pipes = []
        file =  options["pathfileconfig"]
	fileconfig = HSConfig::load(file)
	log = HSConfig::log_setup(fileconfig)
        self.update_plist(options,fileconfig,log) 
	completed


    end


end

