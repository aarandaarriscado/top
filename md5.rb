#!/usr/bin/env ruby
# Encoding: utf-8


require "digest/md5"



class MD5
  attr_accessor :md5


  def initialize (file)

    long_buffer= 5*1024*1024

    if (File.open(file))then
      myFile = File.open (file)
    end

    tamañoArchivo= File.size(myFile)

    if tamañoArchivo < 10*1024*1024 then
      @md5= Digest::MD5.hexdigest(File.read(myFile))

      myFile.close

    else

      parte1=File.size(myFile).to_s   #Se coge el tamaño del archivo

      myFile.seek(0,IO::SEEK_SET)  # Se coge los primeros "long_buffer" bytes del archivo
      parte2= myFile.read(long_buffer)


      myFile.seek(-1*long_buffer, IO::SEEK_END)  # Se coge los ultimos "long_buffer" bytes del archivo
      parte3= myFile.read(long_buffer)

      myFile.close
      ### Se transcodifica los 3 bloques juntos en formato MD5

      trans = Digest::MD5.new
      trans.update(parte1)
      trans.update(parte2)
      trans.update(parte3)

	
      @md5= trans.hexdigest



    end

  end
end
