#!/usr/bin/env ruby
# Encoding: utf-8
require 'rubygems'
require 'xmlsimple'
require 'xml/libxml'
require 'fileutils'
require 'net/smtp'
require_relative 'Enviamail'
require_relative 'hsconfig'

class XmlParser  
    attr_accessor :publiserid
    attr_accessor :accountname
    attr_accessor :mail
    attr_accessor :video
    attr_accessor :audio
    attr_accessor :name
    attr_accessor :videofullrefid 
    attr_accessor :audiofullrefid
    attr_accessor :active
    attr_accessor :refidvideo
    attr_accessor :priority
    attr_accessor :description
    attr_accessor :tag
    attr_accessor :videostillrefid
    attr_accessor :thumbnailrefid
    attr_accessor :thumbsecond 
    attr_accessor :stillsecond
    attr_accessor :update
    attr_accessor :audiostillrefid
    attr_accessor :assetvideofullrefid
    attr_accessor :assetvideofilename
    attr_accessor :assetvideoencodeto
    attr_accessor :assetvideoencodemultiple
    attr_accessor :assetvideodisplayname
    attr_accessor :assetvideotype
    attr_accessor :assetvideosize
    attr_accessor :assetstillrefid
    attr_accessor :assetstillfilename
    attr_accessor :assetstilldisplayname
    attr_accessor :assetstilltype
    attr_accessor :assetstillsize
    attr_accessor :assetthumbrefid
    attr_accessor :assetthumbfilename
    attr_accessor :assetthumbdisplayname
    attr_accessor :assetthumbtype
    attr_accessor :assetthumbsize
    attr_accessor :logo
    attr_accessor :xml
    attr_accessor :id
    attr_accessor :value
    attr_accessor :publication_start_date
    attr_accessor :publication_end_date

    def initialize(xmlpath)  
     # Instance variables  
     
     fileconfig = HSConfig::load("/PFC/TOP/confs/general.yaml")
     log = HSConfig::log_setup(fileconfig)

     begin
	    @xml = XmlSimple.xml_in(xmlpath, { 'KeyAttr' => 'anon' })
     	dtd = XML::Dtd.new(File.read("/PFC/TOP/confs/xmltop.dtd"))
     	doc = XML::Document.file(xmlpath)
	    @mail = xml["notify"][0]["email"]
     	doc.validate(dtd)

     rescue Exception => e
	     puts e.to_s
	    puts "loquesea" 
       	FileUtils.mv  xmlpath , "/jobfilejob/videos/TOP/error/" + File.basename(xmlpath)
	log.error ( "Fichero xml mal formado " + File.basename(xmlpath) + "La excepcion declarada es " + e.to_s)
	Thread.exit
	
 
     end


     #@xml = XmlSimple.xml_in(xmlpath, { 'KeyAttr' => 'anon' })
     @publiserid = xml["gublisher-id"]
     @accountname = xml["acount-name"]


     @video = xml["video"][0]
     @videofullrefid =  xml["video"][0]["video-refid"]

     	@active = xml["video"][0]["active"]
	if @active == "TRUE" then
        	@active = 1
	else
        	@active = 0
        end

        @refidvideo = xml["video"][0]["refid"]
	@priority = xml["video"][0]["priority"]

        #if @refid.match(/\s/) then
        if !@refidvideo.match(/^[A-Za-z0-9_.]+$/) then
                     FileUtils.mv  xmlpath , "/jobfilejob/videos/TOP/error/" + File.basename(xmlpath)
                     msgstr = "Subject: TOP JOB ERROR: El Refid del video ha de ser un string sin espacios y compuesto unicamente por letras en mayusculas y minusculas" + "\n"  + xml.to_s
         	     message = Enviamail.new(msgstr,@mail)
        	     Thread.exit
        end

        begin
            	     @description = xml["video"][0]["description"][0].to_s
        rescue
             	     @description = nil 
        end



        ######### Asignación de detalles del xml

        begin
        	     @name = xml["video"][0]["name"][0].to_s

        rescue
	             msgstr = "Subject: TOP JOB ERROR: No se ha introducido el parametro cdata en el nombre del video" + "\n"  + xml.to_s
	             message = Enviamail.new(msgstr,@mail)
	             FileUtils.mv  xmlpath , "/jobfilejob/videos/TOP/error/" + File.basename(xmlpath)
	             Thread.exit

        end

        @videostillrefid = xml["video"][0]["still-refid"]
        @thumbnailrefid = xml["video"][0]["thumbnail-refid"]
        @thumbsecond = xml["video"][0]["thumb-second"]
        @stillsecond = xml["video"][0]["still-second"]
        @update = xml["video"][0]["update"]
        begin
        	     @geobloqueo = xml["video"][0]["geo-restricted"].to_s
        rescue #### Si no vienen parametros de geo-bloqueo al no ser obligatorios seguimos con la ejecucion
   		     @geobloqueo = nil
        end
	      begin
		     @logo = xml["video"][0]["logo"]
        rescue #### Si no vienen parametros de geo-bloqueo al no ser obligatorios seguimos con la ejecucion
		     @logo = "FALSE"
        end
     	begin
	             assetdata = xml.fetch('asset')
	    rescue
        	     assetdata = []
        end





	assetdata.each do |assets|
           case assets.fetch('type')
               when 'VIDEO' then
                  @assetvideofullrefid = assets["refid"]
                  if   @assetvideofullrefid == @videofullrefid  then
                       @assetvideofilename =  assets["filename"]
                       @assetvideoencodeto = assets["encode-to"]
                       @assetvideoencodemultiple = assets["encode-multiple"]
                       @assetvideotype = assets["type"]
                       @assetvideosize = assets["size"]
                       if @assetvideosize == nil        then
                                @assetvideosize = "0"
                       end

                  else
                        FileUtils.mv  xmlpath , "/jobfilejob/videos/TOP/error/" + File.basename(xmlpath)
                        msgstr = "Subject: TOP JOB ERROR: No coincide el refid del video con el declarado en el asset" + "\n"  + xml.to_s
                        message = Enviamail.new(msgstr,@mail)
                        Thread.exit
                  end


               when 'STILL' then
                  @assetstillrefid = assets["refid"]
                  if   @assetstillrefid == @videostillrefid  then
                       @assetstillfilename =  assets["filename"]
                       @assetstilldisplayname = assets["display-name"]
                       @assetstilltype = assets["type"]
                       @assetstillsize = assets["size"]

                  else
                        FileUtils.mv  xmlpath , "/jobfilejob/videos/TOP/error/" + File.basename(xmlpath)
                        msgstr = "Subject: TOP JOB ERROR: No coincide el refid de la Stillimage con el declarado en el asset" + "\n"  + xml.to_s
                        message = Enviamail.new(msgstr,@mail)
                        Thread.exit
                  end

               when 'THUMBNAIL' then
                  @assetthumbrefid = assets["refid"]
                  if @assetthumbrefid == thumbnailrefid then
                       @assetthumbfilename =  assets["filename"]
                       @assetthumbdisplayname = assets["display-name"]
                       @assetthumbtype = assets["type"]
                       @assetthumbsize = assets["size"]
                  else
                       FileUtils.mv  xmlpath , "/jobfilejob/videos/TOP/error/" + File.basename(xmlpath)
                       msgstr = "Subject: TOP JOB ERROR: No coincide el refid de la Thumbimage con el declarado en el asset" + "\n"  + xml.to_s
                       message = Enviamail.new(msgstr,@mail)
                       Thread.exit
                  end

           end ### Fin del Case

     	end ### Fin del for de los assets
            #### Parseamos la informacion de los assets remotos


     end ### Fin del If de distincion de video de audio


     if @description == "{}" then
	      @description = nil
     end	

  end #### Finalizacion Initialize 
  
