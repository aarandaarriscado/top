#!/usr/bin/env ruby
# Encoding: utf-8
require 'sinatra/base'
require 'resque'
require 'open3'
require 'resque/server'
require 'resque/status_server'
require 'resque/job_with_status'
require 'xmlsimple'
require 'fileutils'
require 'net/ftp'
require 'date'
require 'json'
#require 'resque-retry'
require_relative 'hsconfig.rb'
class Uploadimage
  include Resque::Plugins::Status
  #extend Resque::Plugins::Retry
  #@retry_limit = 3
  #@retry_delay = 120

  def generatefile(video)
	imagedst = File.dirname(video) + "/" + File.basename(video,File.extname(video)) + "_"  + Time.now.to_i.to_s  + ".jpg"
	return imagedst	
	
  end

  def generatecmd(video,imagedst,second,log,rotation)

         second = "3" if second.nil?
         cmd="ffmpeg -i '" + video +  "' -ss 00:00:" + second + " -y -f image2 -vframes 1 -vf scale=320:180 " + imagedst
         return cmd

  end




  def generate_image(cmd,log)
     # Instance variables
	Open3.popen2(cmd) do |stdin, stdout, wait_thr|
          @stop_stdin = stdin
           stdout.each("\r") do |line|
                if line =~ /ffmpeg/i
                        #@log.debug("Encoder #{encoding_profile}: #{line}")
                end

                if line =~ /error/i
                          log.info  ("Generando el poster  hemos encontrado un error no fatal")
                end
           end

         pid = wait_thr.pid
         exit_status = wait_thr.value
         if exit_status != 0 then
                log.info ("Error generando la imagen ejecutando el siguiente comando")
		log.info (cmd)
		raise "error generando la imagen" 
         end
	 log.info (cmd)
         end

    end #### Finalizacion Initialize



  def upload_file(options,imagedst,fileconfig,log) 
		urlsconf = fileconfig["urlbaseim"]
		
		folder2 = fileconfig["uploadpath"] + options["monthpath"] + "/" + options["daypath"] 
		FileUtils.mkdir_p  folder2
   
		if options["file"] == nil  then
                                        sourcefile =  imagedst
                                        ftpdest = folder2 + "/" +  File.basename(imagedst)
                                        url =  urlsconf +  options["monthpath"] + "/" + options["daypath"] +  "/" + File.basename(imagedst)
                else
                                        sourcefile =  fileconfig["dirworking"] +  options["file"]
                                        ftpdest = folder2 + "/" + options["file"]
                                        url =  urlsconf +  options["monthpath"] + "/" + options["daypath"] +  "/" + options["file"]
                end


				
				log.info (options["file"])
                                log.info ("Uploadimage: Subiendo el siguiente fichero al ftp")
				log.info (sourcefile)
				log.info (ftpdest)
				
				FileUtils.cp sourcefile, ftpdest

                ##Actualizamos la rendition
        	
				
                
				metadatafile = fileconfig["uploadjson"] + options["idref"] + ".json"
				hash = JSON.parse(File.read(metadatafile))
				hash["url_video_still"] = url
				File.open(metadatafile,"w") do |f|
					f.puts hash.to_json
				end	
	
	





   end

   

   def perform
        encoding_pipes = []
        file =  options["pathfileconfig"]
	fileconfig = HSConfig::load(file)
	log = HSConfig::log_setup(fileconfig)
	if options["file"] == nil then
			imagedst = self.generatefile(options["video"])
			cmd = self.generatecmd(options["video"],imagedst,options["second"],log,options["rotation"])
                        self.generate_image(cmd,log)
        end

	self.upload_file(options,imagedst,fileconfig,log) 
	completed


    end


end

