
## TOP

Diseño de un sistema automatizado de publicación de video a través de un gestor de colas

Proyecto Final de Grado. Grado en informática 

Resumen
 La evolución de los hábitos de consumo de información por parte de los Millennials  se posiciona al video como el medio más intuitivo y eficaz para dar a la gente más  información. En base al estudio de las soluciones de transcodificación actuales, se propone la implementación de una cola de transcodificación que permita aumentar la eficiencia operativa de nuestro sistema. Este trabajo  utiliza ffmpeg como herramienta de transcodificacion junto con la base de datos Redis para crear y gestionar colas de ejecución de forma que se reduce la presión sobre los servidores 
Palabras Clave: ffmpeg, queue , redis , transcodificacion , video
