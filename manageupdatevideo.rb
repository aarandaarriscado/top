require 'sinatra/base'
require 'resque'
require 'open3'
require 'resque/server'
require 'resque/status_server'
require 'resque/job_with_status'
require 'xmlsimple'
require 'fileutils'
require 'net/ftp'
require 'date'
require 'json'
require 'uri'
require_relative 'Enviamail'
require_relative 'manageimage.rb'
require_relative 'top.rb'
require_relative 'uploadtop.rb'
require_relative 'ScheduleQueue.rb'
module Manageupdatevideo 

	def Manageupdatevideo.gestion(d,log,pathxml,file,fileconfig) 
        	p "Estamos en manageupdatevideo"
		pathfileconf = $DirConf + d.accountname + ".yaml"
		puts pathfileconf 
		if !d.assetvideofilename.nil? and !d.assetvideofullrefid.nil? then

                	######Generamos sha1 del fichero de entrada
	                ###pathold = ($Dirmonitor + d.assetvideofilename)
			###newpathdone = d.refidvideo + File.extname(($Dirmonitor + d.assetvideofilename))
	                ####d.assetvideofilename = newpathdone
        	        path = $Dirmonitor + d.assetvideofilename
	                ##############################################
			p "estamos generando shal"
        	        if File.exist?(path)
				p "nos vamos a meter en Metadatavideo"
                	         sourcedata = Metadatavideo.new(path,fileconfig["templatemediainfo"],fileconfig["templatemediainforotation"],d.assetvideosize,pathxml,fileconfig,log)
				source = {"bitrate" => sourcedata.vbitrate.to_i , "codec" => sourcedata.vcodec , "container" => sourcedata.format , "width" => sourcedata.width.to_i , "height" => sourcedata.height.to_i , "size" => sourcedata.size.to_i} 
				p "Metadata correctamente realizado"
	                else
        	                log.error ("No existe el fichero " +  path + "Para el xml de entrada " + pathxml)
                	        msgstr = "Subject: TOP JOB ERROR: No existe el fichero media de entrada declarado en el xml" + "\n"  + d.xml.to_s
                        	message = Enviamail.new(msgstr,d.mail)
                        	FileUtils.mv    pathxml  , fileconfig["direrror"] + File.basename(pathxml)
	                        Thread.exit
        	        end



		end
		log.info "Comprobaciones"
		log.info d.assetvideofilename
		log.info d.assetvideofullrefid
		log.info 
		if !d.assetvideofilename.nil? and !d.assetvideofullrefid.nil? then
                	input = fileconfig["dirworking"] + d.assetvideofilename
			log.info "Vamos a ejecutar en Preparertranscoder para crear comandos de transcodificacion"

			



			################MODIFICACION
			 case d.assetvideoencodeto
                          when 'MP4' then
				
                            log.info "hemos entrado al preparetranscoder en la opcion de MP4 en un Update"
                            input = fileconfig["dirworking"] + d.assetvideofilename
                            assets = Preparertranscoder.new(d,fileconfig,input,sourcedata,log)
                          when 'none' then
                            log.info "hemos entrado al preparetranscoder en la opcion de none en un Update"
                            input = fileconfig["dirworking"] + d.assetvideofilename
                            assets = Preparertranscoder.new(d,fileconfig,input,sourcedata,log)

                          when nil then
                            log.info "hemos entrado al preparetranscoder en la opcion de nil en un Update"
                            input = fileconfig["dirworking"] + d.assetvideofilename
                            assets = Preparertranscoder.new(d,fileconfig,input,sourcedata,log)
			end
		    	       



		 	source = {"bitrate" => sourcedata.vbitrate.to_i , "codec" => sourcedata.vcodec , "container" => sourcedata.format , "width" => sourcedata.width.to_i , "height" => sourcedata.height.to_i , "size" => sourcedata.size.to_i , "md5Original" => sourcedata.md5}
                	videorest = {"idref" =>d.refidvideo , "name" => d.name,  "account" => d.publiserid, "description" => d.description , "mail" => d.mail ,"active" => d.active , "publicationDateStart" => d.publication_start_date, "publicationDateEnd" => d.publication_end_date, "status" => "1" , "provider" =>  fileconfig["provider"] ,"beginOfProcess" => Time.now.to_i , "length" => sourcedata.duration , "source" => source}
	                FileUtils.mv  path , fileconfig["dirworking"] + d.assetvideofilename 
        	else
                        if !d.assetvideofilename.nil? and File.exist?(path)
				videorest = {"idref" =>d.refidvideo , "name" => d.name,  "account" => d.publiserid, "description" => d.description , "publicationDateStart" => d.publication_start_date, "publicationDateEnd" => d.publication_end_date,  "beginOfProcess" => Time.now.to_i , "active" => d.active, "length" => sourcedata.duration , "source" => source}
			else
				videorest = {"idref" =>d.refidvideo , "name" => d.name,  "account" => d.publiserid, "description" => d.description ,  "publicationDateStart" => d.publication_start_date, "publicationDateEnd" => d.publication_end_date,  "beginOfProcess" => Time.now.to_i , "active" => d.active}
			end
			log.info ("Vamos a ejecutar en Preparertranscoder para sincrear comandos de transcodificacion")
                        assets =  Preparertranscoder.new(d,fileconfig,nil,nil,log)
                        input = nil
                end
		

	
		videorest.delete_if { |k, v| v.nil? }

			
			log.info "Calculamos options con video de entrada"
            options = {"idref" =>d.refidvideo ,"assetvideofilename" => d.assetvideofilename, "assetvideofullrefid" => d.assetvideofullrefid,  "accountname" => d.accountname ,"pathfileconfig" => pathfileconf , "video" => input ,  "second" => d.stillsecond , "stillimage" => d.assetstillfilename, "thumbimage" => d.assetthumbfilename  ,  "rotation" => "0" , "monthpath" => assets.monthpath , "daypath" => assets.daypath , "duration" =>  sourcedata.duration , "framerate" => sourcedata.vframerate ,  "update" => d.update , "datarest" => videorest , "data" => assets.renditions , "assetscount" => assets.count }
      

		
 
		FileUtils.mv  $Dirmonitor + file , fileconfig["dirworking"] + file

		log.info "Hemos movido el xml de entrada"

	         if !d.assetstillfilename.nil? then
            	    	log.info "Vamos a mover el fichero de video"
			begin
                	        FileUtils.mv  $Dirmonitor + d.assetstillfilename , fileconfig["dirworking"] + d.assetstillfilename
				
	            	rescue
                       		d.assetstillfilename = nil
                    	end
        	 end
 	         if !d.assetthumbfilename.nil? then
            		begin
                        	FileUtils.mv  $Dirmonitor + d.assetthumbfilename , fileconfig["dirworking"] + d.assetthumbfilename
	                rescue
        	                d.assetthumbfilename = nil
                	end
        	 end
		 
	        ###Generamos los jobs para subir las imagenes
	        log.info  "Vamos llamar al proceso ManageImage"

        ##############Modificacion condiciones de carrera options --> optionsimage
		if input != nil then

                        log.info "Calculamos options con video de entrada"
                        optionsimage = {"idref" =>d.refidvideo ,"assetvideofilename" => d.assetvideofilename, "assetvideofullrefid" => d.assetvideofullrefid,  "accountname" => d.accountname ,"pathfileconfig" => pathfileconf , "video" => input ,  "second" => d.stillsecond , "stillimage" => d.assetstillfilename, "thumbimage" => d.assetthumbfilename  ,  "rotation" => "0" , "monthpath" => assets.monthpath ,  "daypath" => assets.daypath , "duration" =>  sourcedata.duration , "framerate" => sourcedata.vframerate ,  "update" => d.update , "datarest" => videorest , "data" => assets.renditions , "assetscount" => assets.count }

                else
                        log.info "Calculamos options sin video de entrada"
                        optionsimage = {"idref" =>  d.refidvideo ,  "accountname" => d.accountname ,"pathfileconfig" => pathfileconf , "video" => input , "second" =>  d.stillsecond ,  "stillimage" => d.assetstillfilename, "thumbimage" => d.assetthumbfilename  , "monthpath" =>  assets.monthpath , "daypath" =>  assets.daypath , "update" => d.update, "datarest" => videorest  }

                end

		
 		Manageimage.update_image(optionsimage,log)
 
		log.info "Hemos acabado con el manageimage"
		#if d.daylimotion == "TRUE" and !d.assetvideofilename.nil?
		#	options = {"idref" =>d.refidvideo , "accountname" => d.accountname ,"pathfileconfig" => pathfileconf, "video" => input , "name" => d.name , "description" => d.description , "dailymotionidref" => d.idrefdailimotion}
		#	queuename = "uploaddaylimotion"
		#	Uploaddaylimotion.enqueue_to(queuename,Uploaddaylimotion,options)
		#end 

		#Generamos el json de salida
		metadatafile = fileconfig["uploadjson"] + d.refidvideo + ".json"
		File.open(metadatafile,"w") do |f|
			f.write(videorest.to_json)
		end
		
       		i = 0
	        #Generamos los inserts de transcodificacion
		log.info "Vamos a generar las renditions"
		accountpriority = fileconfig["priority"]
        	unless assets.renditions.nil?
                	while i < assets.renditions.count
                        	options = {"idref" =>d.refidvideo , "accountname" => d.accountname ,"pathfileconfig" => pathfileconf , "data" => assets.renditions[i] , "assetscount" => assets.count, "update" => d.update}
				
				priority = ScheduleQueue.new(accountpriority,d.priority,sourcedata.duration,i)
				i = i + 1
				Top.enqueue_to(priority.priority,Top,options)
                end
		Thread.exit
        end

 

	end

end
