#!/usr/bin/env ruby
require 'sinatra/base'
require 'resque'
require 'open3'
require 'resque/server'
require 'resque/status_server'
require 'xmlsimple'
require 'resque/job_with_status'
require 'fileutils'
require 'net/ftp'
require 'date'
require 'json'
require_relative 'hsconfig.rb'
require_relative 'Calculeplaylist.rb'
require_relative 'Enviamail'
require_relative 'updateplist'
class Updatecms
  include Resque::Plugins::Status

   def update_metadata(options,fileconfig,log)

                                ##Actualizamos la rendition

                                urlsconf = fileconfig["url"]
                                url = []
                                type = []
                                u = 0
				log.info (urlsconf.count)
                                while u < urlsconf.count
                                        url[u] = {"url" => fileconfig[urlsconf[u]][0] +  options["data"]["urlmonthpath"] + "/" + options["data"]["urldaypath"]  + "/" + File.basename(options["data"]["dst"]) + fileconfig[urlsconf[u]][1] , "typeUrl" => fileconfig[urlsconf[u]][2]}

                                        urlbase = fileconfig[urlsconf[u]][0] + options["data"]["urlmonthpath"] + "/" + options["data"]["urldaypath"] + "/" + File.basename(options["data"]["src"],File.extname(options["data"]["src"])) + "_"
                                        urlext =  File.extname(options["data"]["src"]) + fileconfig[urlsconf[u]][1]
                                        type[u] = {"urlbase" => urlbase , "urlext" => urlext }
					log.info (url)
                                        u = u + 1

                                end
				
                                command = options["data"]["command"]
                                src = options["data"]["src"]
                                vcodec = options["data"]["vcodec"]
                                vbitrate = options ["data"]["vbitrate"]
                                container = options["data"]["container"]
                                width = options["data"]["width"]
                                height = options["data"]["height"]
				mimetype = options["data"]["mimetype"]
				size = File.size(options["data"]["dst"])
                                assetdata = { "src" => src , "bitrate" => vbitrate , "codec" => vcodec , "mimetype" => mimetype ,"size" => size , "container" => container , "width" => width , "height" => height , "url" => url }
                                log.info ( "insertando el asset ")
				
				log.info ( assetdata.to_json)
				metadatafile = fileconfig["uploadjson"] + options["idref"] + ".json"
				hash = JSON.parse(File.read(metadatafile))
				
			        arrayassets = []	
				if !hash["asset"].nil? then
					i = hash["asset"].count
					arrayassets[0] = hash["asset"][0]
					arrayassets[1] = assetdata
				else
					arrayassets[0] = assetdata
				end	
				status = hash["status"]    #anteriormente era playlist = Calculeplaylist.new(type,urlapiread,options["assetscount"])
				hash["asset"] = arrayassets 
				
				File.open(metadatafile,"w") do |f|
					f.puts hash.to_json
				end	
				queuename = "metadata" 
		        Updateplist.enqueue_to(queuename,Updateplist,options)



   end

   

   def perform
        encoding_pipes = []
        file =  options["pathfileconfig"]
	fileconfig = HSConfig::load(file)
	log = HSConfig::log_setup(fileconfig)
        self.update_metadata(options,fileconfig,log) 
	completed


    end


end

