#!/usr/bin/env ruby
# Encoding: utf-8
require 'rubygems'
require 'xmlsimple'
require 'resque'
require 'resque/status_server'
require 'resque/job_with_status'
require 'active_support'
require 'open4'
require 'fileutils'
require_relative 'hsconfig.rb'
require_relative 'XmlParser.rb'
require_relative 'Metadatavideo.rb'
require_relative 'Preparertranscoder.rb'
require_relative 'top.rb'
require_relative 'Calculeplaylist.rb'
require_relative 'uploadtop.rb'
require_relative 'uploadimage.rb'
require_relative 'uploadthumb.rb'
require_relative 'Enviamail'
require_relative 'managevideo'
require_relative 'manageupdatevideo'
require 'json'
require 'digest'
#require 'restclient'
require 'digest/sha1'


if ARGV.empty?
  puts "Usage: Se ha de insertar la carpeta a monitorizar ."
  exit 1
end

$Dirmonitor=ARGV[0]
$processing="processing/"
$DirConf="/PFC/TOP/confs/"


$lock="/jobfilejob/lock"

if !File.exist?($lock) then

fh = File.open($lock, File::CREAT)
threads = []

Dir.foreach($Dirmonitor) do |f|
  next if f == '.' or f == '..'  or !(File.extname(f) == '.xml') or (f.include? "_tasks.xml")
  threads  << Thread.new(f) do |file| 
  	

	#### Parseamos el fichero de entrada ####
	pathxml=($Dirmonitor + file) 
	d = XmlParser.new(pathxml)  
	#### Fin del parseo del xml de entrada

	####Cargamos el fichero de configuraciones debido a que el xml esta bien formado
        pathfileconf = $DirConf + d.accountname + ".yaml"
        fileconfig = HSConfig::load(pathfileconf)
        ##### Fin de la carga del fichero de configuraciones

	#### Carga del archivo de log
	log = HSConfig::log_setup(fileconfig)
	log.info ("Parseado el fichero e iniciado el programa para el xml " + pathxml) 

  	#### Fin de la carga del archivo de log

	if !d.refidvideo.nil?  then
		
		#puts "comenzamos a tratar u update"
		Managevideo.gestion(d,log,pathxml,file)

	end
	
  	






  end  	
	

end

threads.each {  |t| t.join }

File.delete($lock)
end

puts "No existen ficheros pendientes de procesar"

