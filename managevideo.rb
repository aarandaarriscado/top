require_relative 'Enviamail'
require_relative 'top.rb'

module Managevideo 

	def Managevideo.gestion(d,log,pathxml,file) 
		
        	####Cargamos el fichero de audios debido a que el xml esta bien formado
        	pathfileconf = $DirConf + d.accountname + ".yaml"
		fileconfig = HSConfig::load(pathfileconf)

		##### Fin de la carga del fichero de configuraciones
		
		if d.update == "TRUE" then

                	#puts "comenzamos a tratar u update"
                	Manageupdatevideo.gestion(d,log,pathxml,file,fileconfig)

  	        end

		      ##### Obtenemos los datos de origen del video
	        if !d.assetvideofilename.nil? and !d.assetvideofullrefid.nil? then

               		 ######Generamos sha1 del fichero de entrada
        	         pathold = ($Dirmonitor + d.assetvideofilename)
       		         ##semilla = "secrettop" + d.assetvideofilename
                	 ##newpath =  Digest::MD5.hexdigest(semilla)
                	 ##newpathdone = newpath + File.extname(($Dirmonitor + d.assetvideofilename))
                	 ##d.assetvideofilename = newpathdone
                	 newpathdone = d.refidvideo + File.extname(($Dirmonitor + d.assetvideofilename))
                	 d.assetvideofilename = newpathdone
                	 path = $Dirmonitor + newpathdone
                	 ##############################################

                	 if File.exist?(pathold)
                        	 sourcedata = Metadatavideo.new(pathold,fileconfig["templatemediainfo"],fileconfig["templatemediainforotation"],d.assetvideosize,pathxml,fileconfig,log)
				 FileUtils.mv  pathold , fileconfig["dirworking"] + d.assetvideofilename
                	 else
                        	log.error ("No existe el fichero " +  pathold + "Para el xml de entrada " + pathxml)
                        	msgstr = "Subject: TOP JOB ERROR: No existe el fichero media de entrada declarado en el xml" + "\n"  + d.xml.to_s
                        	message = Enviamail.new(msgstr,d.mail)
                        	FileUtils.mv    pathxml  , fileconfig["direrror"] + File.basename(pathxml)
                        	Thread.exit
                	end
        	end

		p d.assetvideoencodeto.class
		if !d.assetvideofilename.nil? and !d.assetvideofullrefid.nil? then
		 case d.assetvideoencodeto 
        	       when 'MP4' then
			 p "hemos entrado al preparetranscoder en la opcion de MP4"
                	  input = fileconfig["dirworking"] + d.assetvideofilename
                          assets = Preparertranscoder.new(d,fileconfig,input,sourcedata,log)
              	       when 'none' then
			 p "hemos entrado al preparetranscoder en la opcion de none"
                 	  input = fileconfig["dirworking"] + d.assetvideofilename
                        assets = Preparertranscoder.new(d,fileconfig,input,sourcedata,log)

              	       when nil then
			 p "hemos entrado al preparetranscoder en la opcion de nil"
                 	  input = fileconfig["dirworking"] + d.assetvideofilename
                        assets = Preparertranscoder.new(d,fileconfig,input,sourcedata,log)
		      
		       else   
			assets = []
                        log.error ("No hemos recibido ningun asset ni ningun asset remoto en el xml"  + pathxml)
                        FileUtils.mv    pathxml  , fileconfig["direrror"] + File.basename(pathxml)
                        msgstr = "Subject: TOP JOB ERROR: No se han definido assets ni assets remotos en el xml" + "\n"  + d.xml.to_s
                        message = Enviamail.new(msgstr,d.mail)
                        Thread.exit 
              	 end

		end

		if !d.assetstillfilename.nil? then
                	begin
                        	FileUtils.mv  $Dirmonitor + d.assetstillfilename , fileconfig["dirworking"] + d.assetstillfilename
                	rescue
                       		 d.assetstillfilename = nil
                	end
        	end
        	if !d.assetthumbfilename.nil? then
                	begin
                        	FileUtils.mv  $Dirmonitor + d.assetthumbfilename , fileconfig["dirworking"] + d.assetthumbfilename
                	rescue
                        	d.assetthumbfilename = nil
                	end
       		 end

		source = {"bitrate" => sourcedata.vbitrate.to_i , "codec" => sourcedata.vcodec , "container" => sourcedata.format , "width" => sourcedata.width.to_i , "height" => sourcedata.height.to_i , "size" => sourcedata.size.to_i , "md5Original" => sourcedata.md5}
        datarest = {"idref" =>d.refidvideo , "name" => d.name,  "account" => d.publiserid, "description" => d.description , "ptags" => d.ptag , "mail" => d.mail ,"active" => d.active , "publicationDateStart" => d.publication_start_date, "publicationEndDate" => d.publication_end_date, "season" => d.season, "chapter" => d.chapter, "status" => "1" , "provider" =>  fileconfig["provider"] ,"beginOfProcess" => Time.now.to_i , "length" => sourcedata.duration , "source" => source, "location" => d.geo }
        apiweb = fileconfig["apibase"] + "/"  + fileconfig["apitoken"]
        datarest.delete_if { |k, v| v.nil? }

	        
		optionslocal = {"idref" =>d.refidvideo , "accountname" => d.accountname ,"pathfileconfig" => pathfileconf , "video" => input ,  "second" => d.stillsecond , "stillimage" => d.assetstillfilename, "thumbimage" => d.assetthumbfilename  ,  "rotation" => "0" , "monthpath" => assets.monthpath ,"urlgeo" => assets.geopath  , "daypath" => assets.daypath , "duration" =>  sourcedata.duration , "framerate" => sourcedata.vframerate ,  "update" => d.update , "datarest" => datarest , "data" => assets.renditions , "assetscount" => assets.count }


				
		FileUtils.mv  pathxml , fileconfig["dirworking"] + file

		SetInsertVideo.enqueue_to(fileconfig["queue"],SetInsertVideo,optionslocal)

		Thread.exit

	end

end
