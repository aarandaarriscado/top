require 'logger'
require 'yaml'

class HSConfig

  def self.load( config_file )
    HSConfig.new( config_file )
  end

  def initialize(config_file)
    @config = YAML::load(File.open(config_file))
    #puts @config['log_type']
    #puts @config['temp_dir']
    #sanity_check(@config)
  end

  def [](index)
    @config[index]
  end

  def self.log_setup(config)
    if config['log_type'] == 'FILE'
      log = Logger.new(config['log_file'])
    else
      log = Logger.new(STDOUT)
    end

    case config['log_level']
      when 'DEBUG'
        log.level = Logger::DEBUG
      when 'INFO'
        log.level = Logger::INFO
      when 'WARN'
        log.level = Logger::WARN
      when 'ERROR'
        log.level = Logger::ERROR
      else
        log.level = Logger::DEBUG
    end

    return log
  end

  private

  def sanity_check(config)
    log = HSConfig::log_setup(config)




  end

end
