#!/usr/bin/env ruby
require 'sinatra/base'
require 'resque'
require 'open3'
require 'resque/server'
require 'resque/status_server'
require 'resque/job_with_status'
require 'xmlsimple'
require 'fileutils'
require 'net/ftp'
require 'date'
require 'json'
require_relative 'hsconfig.rb'
require_relative 'Calculeplaylist.rb'
require_relative 'Enviamail'
require_relative 'updateplist'
module Manageimage 
  include Resque::Plugins::Status

   def Manageimage.insert_image(options, log)

			#optionslocal=options
				log.info(options)
               			log.info('#############################################')
 
				if !options["stillimage"].nil? or !options["video"].nil? then

                        	        log.info "Comenzamos a realizar el insert de la imagen  "
                                	 optionsimage = {"idref" =>  options["idref"] ,  "accountname" => options["accountname"] ,"pathfileconfig" => options["pathfileconfig"] , "video" => options["video"] , "second" =>  options["second"] , "file" =>  options["stillimage"], "monthpath" =>  options["monthpath"]  , "daypath" =>  options["daypath"] , "update" => options["update"] }
                                	 queuename = "images"
                                	 Uploadimage.enqueue_to(queuename,Uploadimage,optionsimage)

                 		end

		    		 if !options["thumbimage"].nil? or !options["video"].nil? then

                                	log.info "Comenzamos a realizar el insert de la imagen del audio"
                                 	optionsthumb = {"idref" =>  options["idref"] ,  "accountname" => options["accountname"] ,"pathfileconfig" => options["pathfileconfig"] , "video" => options["video"] , "second" =>  options["second"] , "file" =>  options["thumbimage"], "monthpath" =>  options["monthpath"]  , "daypath" =>  options["daypath"] , "update" => options["update"] }
                                 	queuename = "images"
			         	Uploadthumb.enqueue_to(queuename,Uploadthumb,optionsthumb)


                   		 end




   end

   def Manageimage.update_image (options, log)

		#options=optionslocal

		if !options["assetvideofilename"].nil? and !options["assetvideofullrefid"].nil? and !options["video"].nil? then
                        optionsimage = {"idref" =>  options["idref"] ,  "accountname" => options["accountname"] ,"pathfileconfig" => options["pathfileconfig"] , "video" => options["video"] , "second" =>  options["second"] , "file" =>  options["stillimage"],"monthpath" =>  options["monthpath"]  , "daypath" =>  options["daypath"] , "update" => options["update"] }
                        queuename = "images"
                        Uploadimage.enqueue_to(queuename,Uploadimage,optionsimage)


                        optionsthumb = {"idref" =>  options["idref"] ,  "accountname" => options["accountname"] ,"pathfileconfig" => options["pathfileconfig"] , "video" => options["video"] , "second" =>  options["second"] , "file" =>  options["thumbimage"], "monthpath" =>  options["monthpath"]  , "daypath" =>  options["daypath"] , "update" => options["update"] }
                        queuename = "images"
                        Uploadthumb.enqueue_to(queuename,Uploadthumb,optionsthumb)




                else

                        if ! options["thumbimage"].nil? then
                                 optionsthumb = {"idref" =>  options["idref"] ,  "accountname" => options["accountname"] ,"pathfileconfig" => options["pathfileconfig"] , "video" => options["video"] , "second" =>  options["second"] , "file" =>  options["thumbimage"], "monthpath" =>  options["monthpath"]  , "daypath" =>  options["daypath"] , "update" => options["update"] }
                                 queuename = "images"
                                 Uploadthumb.enqueue_to(queuename,Uploadthumb,optionsthumb)
                         end

                         if !options["stillimage"].nil? then
                                 optionsimage ={"idref" =>  options["idref"] ,  "accountname" => options["accountname"] ,"pathfileconfig" => options["pathfileconfig"] , "video" => options["video"] , "second" =>  options["second"] , "file" =>  options["stillimage"], "monthpath" =>  options["monthpath"]  , "daypath" =>  options["daypath"] , "update" => options["update"] }
                                 queuename = "images"
                                 Uploadimage.enqueue_to(queuename,Uploadimage,optionsimage)

                         end

                end

   end



end

